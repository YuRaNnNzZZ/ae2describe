package ru.ffgs.ae2describe.proxy;

import ru.ffgs.ae2describe.item.ItemPart;

public class ClientProxy extends CommonProxy {
	@Override
	public void registerPartModels(ItemPart item) {
		if (item == null) return;

		item.registerModels();
	}
}
