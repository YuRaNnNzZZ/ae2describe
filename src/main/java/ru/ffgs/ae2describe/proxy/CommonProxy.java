package ru.ffgs.ae2describe.proxy;

import ru.ffgs.ae2describe.item.ItemPart;

public abstract class CommonProxy {
	public abstract void registerPartModels(ItemPart item);
}
