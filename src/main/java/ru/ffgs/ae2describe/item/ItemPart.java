package ru.ffgs.ae2describe.item;

import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.ffgs.ae2describe.AE2Describe;

public class ItemPart extends Item {
	private String[] subtypeNames;

	public ItemPart(ResourceLocation name, String[] subtypeNames) {
		this.setRegistryName(name);
		this.setUnlocalizedName(name.toString());

		this.setHasSubtypes(true);
		this.subtypeNames = subtypeNames;

		AE2Describe.proxy.registerPartModels(this);
	}

	@Override
	public String getUnlocalizedName(ItemStack stack) {
		if (this.subtypeNames == null)
			return super.getUnlocalizedName(stack);

		int i = stack.getMetadata();
		return i >= 0 && i < this.subtypeNames.length ? super.getUnlocalizedName(stack) + "_" + this.subtypeNames[i] : super.getUnlocalizedName(stack);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		if (!isInCreativeTab(tab)) return;

		if (this.subtypeNames == null) {
			items.add(new ItemStack(this));
			return;
		}

		for (int i = 0; i < this.subtypeNames.length; i++)
			items.add(new ItemStack(this, 1, i));
	}

	@SideOnly(Side.CLIENT)
	public void registerModels() {
		ModelResourceLocation[] partsModels = new ModelResourceLocation[subtypeNames.length];

		for (int i = 0; i < subtypeNames.length; i++)
			partsModels[i] = new ModelResourceLocation(this.getRegistryName() + "_" + subtypeNames[i], "inventory");

		ModelBakery.registerItemVariants(this, partsModels);

		ModelLoader.setCustomMeshDefinition(this, stack -> {
			if (stack.isEmpty() || partsModels.length <= 0) return null;

			int meta = stack.getMetadata();
			if (meta >= 0 && meta <= partsModels.length)
				return partsModels[meta];

			return null;
		});
	}
}
