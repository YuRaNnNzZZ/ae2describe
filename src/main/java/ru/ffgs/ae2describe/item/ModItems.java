package ru.ffgs.ae2describe.item;

import appeng.api.AEApi;
import appeng.core.CreativeTab;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.GameData;
import ru.ffgs.ae2describe.AE2Describe;

public class ModItems {
	public static ItemPart parts;

	public static void registerItems() {
		parts = new ItemPart(new ResourceLocation(AE2Describe.MOD_ID, "material_processor_assembly"), new String[]{"logic", "calculation", "engineering"});
		parts.setCreativeTab(CreativeTab.instance);

		GameData.register_impl(parts);

		AEApi.instance().definitions().materials().logicProcessor().maybeStack(1).ifPresent(itemStack -> {
			GameRegistry.addSmelting(new ItemStack(parts, 1, 0), itemStack, 0);
		});
		AEApi.instance().definitions().materials().calcProcessor().maybeStack(1).ifPresent(itemStack -> {
			GameRegistry.addSmelting(new ItemStack(parts, 1, 1), itemStack, 0);
		});
		AEApi.instance().definitions().materials().engProcessor().maybeStack(1).ifPresent(itemStack -> {
			GameRegistry.addSmelting(new ItemStack(parts, 1, 2), itemStack, 0);
		});
	}
}
