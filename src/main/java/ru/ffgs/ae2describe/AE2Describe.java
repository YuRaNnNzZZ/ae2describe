package ru.ffgs.ae2describe;

import appeng.core.AppEng;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.ffgs.ae2describe.item.ModItems;
import ru.ffgs.ae2describe.proxy.CommonProxy;

@Mod(
		modid = AE2Describe.MOD_ID,
		name = AE2Describe.MOD_NAME,
		version = AE2Describe.MOD_VERSION,
		dependencies = "required-after:" + AppEng.MOD_ID + ";"
)
public class AE2Describe {
	public static final String MOD_ID = "ae2describe";
	public static final String MOD_NAME = "AE2 Describe";
	public static final String MOD_VERSION = "@VERSION@";

	@SidedProxy(clientSide = "ru.ffgs.ae2describe.proxy.ClientProxy", serverSide = "ru.ffgs.ae2describe.proxy.ServerProxy")
	public static CommonProxy proxy;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		ModItems.registerItems();
	}
}
